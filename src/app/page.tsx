import Button from '@mui/material/Button';

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="bg-gray-200 p-8 h-96">
        <h1 className="text-4xl font-bold text-blue-700">Mi aplicación con Next.js, Tailwind CSS y Material-UI</h1>
        <p className="mt-4 text-gray-800">¡Bienvenido al mundo de las tecnologias!</p>
      </div>
    </main>
  )
}
