// components/Menu.tsx
"use client"
import React, { useState } from 'react';
import Image from 'next/image';

const Menu: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <div className="bg-gray-800 p-4 text-white">
      <div className="flex items-center justify-between w-full"> {/* Cambio aquí: justify-between e items-center */}
        {/* Opciones de menú (ocultas en dispositivos móviles) */}
        

        {/* Isotipo de la marca con el componente Image */}
        <div
          style={{
            position: 'relative',
            width: '48px',  // Ajusta el ancho del contenedor según tus necesidades
            height: '48px', // Ajusta la altura del contenedor según tus necesidades
          }}
        >
          <Image
            src="/assets/images/logo.png" // Ruta relativa a la carpeta 'public'
            alt="Logo"
            layout="fill"  // Hace que la imagen ocupe el tamaño del contenedor
            objectFit="contain" // Ajusta la imagen para llenar completamente el contenedor manteniendo la proporción
          />
        </div>
        <div className="hidden md:flex space-x-4 justify-end items-center"> {/* Cambio aquí: justify-end e items-center */}
          <a href="#" className="hover:text-gray-300">
            Welcome
          </a>
          <a href="#" className="hover:text-gray-300">
            About Us
          </a>
          <a href="#" className="hover:text-gray-300">
            Products
          </a>
          <a href="#" className="hover:text-gray-300">
            Contact
          </a>
        </div>
        {/* Botón de menú para dispositivos móviles */}
        <div className="md:hidden">
          <button onClick={toggleMenu} className="text-white">
            <svg
              className="w-6 h-6"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16m-7 6h7"
              />
            </svg>
          </button>
        </div>
      </div>

      {/* Opciones de menú visibles en dispositivos móviles */}
      {isMenuOpen && (
        <div className="md:hidden mt-4 space-y-2 justify-end"> 
        <a href="#" className="block hover:text-gray-300">
          Opción 1
        </a>
        <a href="#" className="block hover:text-gray-300">
          Opción 2
        </a>
        <a href="#" className="block hover:text-gray-300">
          Opción 3
        </a>
        <a href="#" className="block hover:text-gray-300">
          Opción 4
        </a>
      </div>
      )}
    </div>
  );
};

export default Menu;
