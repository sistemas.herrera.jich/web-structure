// components/Footer.tsx

import React from 'react';
import { FaFacebook, FaInstagram, FaTwitter } from 'react-icons/fa';

const Footer: React.FC = () => {
  return (
    <footer className="bg-gray-800 p-4 text-white text-center">
      <div className="flex justify-center space-x-4">
        <a href="#" className="hover:text-gray-300">
          <FaFacebook size={24} />
        </a>
        <a href="#" className="hover:text-gray-300">
          <FaInstagram size={24} />
        </a>
        <a href="#" className="hover:text-gray-300">
          <FaTwitter size={24} />
        </a>
        {/* Agrega más íconos según sea necesario */}
      </div>
      <p className="mt-2">© 2023 Mi Sitio Web</p>
    </footer>
  );
};

export default Footer;
